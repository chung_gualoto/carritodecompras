//
//  CarritoTableViewCell.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 14/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import UIKit

class CarritoTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
