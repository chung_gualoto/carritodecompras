//
//  BackendManager.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 14/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import ObjectMapper
import CoreLocation
class BackendManager {
    
    func guardarItem(id: Int, nombre:String, descripcion:String, detalle: String, imagen: String, precio: Int){
        
        let urlString = "http://localhost:1337/Item/"
        Alamofire.request(urlString, method: .post, parameters:
            [
                "id": id,
                "nombre": nombre,
                "descripcion": descripcion,
                "detalle": detalle,
                "imagen": imagen,
                "precio": precio
            ]
            ).responseJSON { response in
            if let JSON = response.result.value {
                let jsonDict = JSON as! NSDictionary
                print(jsonDict)
            }
        }
    }
    
    func obtenerItems(){
        let urlString = "https://la1.api.riotgames.com/lol/static-data/v3/items?locale=es_MX&tags=all&api_key=RGAPI-d500a9c8-e0b3-42b7-ba46-78a1493f6bc6"
        Alamofire.request(urlString).responseJSON { response in
            if let JSON = response.result.value {
                let jsonDict = JSON as! NSDictionary
                let dataDict = jsonDict.object(forKey: "data") as! NSDictionary
                let itemList: NSMutableArray! = []
                for (_,object) in dataDict {
                    //print(object)
                    itemList.add(Mapper<Item>().map(JSONObject: object)!)
                }
                NotificationCenter.default.post(name:NSNotification.Name("actualizarLista"),object:nil,userInfo:["lista":itemList])
            }
        }
    }
    
    func listarItems(){
        
        let urlString = "http://localhost:1337/Item/"
        Alamofire.request(urlString).responseJSON { response in
            if let JSON = response.result.value {
                let jsonDict = JSON as! NSArray
                //print(jsonDict)
                NotificationCenter.default.post(name:NSNotification.Name("actualizar"),object:nil,userInfo:["lista":jsonDict])
                
            }
        }
    }
    
    func downloadImages(_ imageName:String, imageR: @escaping (UIImage)->()) {
        let URL = "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/item/\(imageName)"
        Alamofire.request(URL).responseImage { response in
            guard let image = response.result.value else{
                return
            }
            imageCache[imageName] = image
            imageR(image)
        }
    }
    
    func delete(_ itemId: Int) {
        let urlString = "http://localhost:1337/Item/\(itemId)"
        Alamofire.request(urlString, method: .delete).responseJSON { response in
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
        }
        
    }
}
