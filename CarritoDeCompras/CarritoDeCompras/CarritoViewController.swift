//
//  CarritoViewController.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 8/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import UIKit

class CarritoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var lista:NSArray = []
    var selectedIndexPath:IndexPath = []
    
    @IBOutlet weak var tablaItems: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar"), object: nil)

        let bm = BackendManager()
        bm.listarItems()
        
    }
    
    func actualizarInformacion(_ notification: Notification){
        print("actualizando")
        lista=notification.userInfo?["lista"] as! NSArray
        //
        self.tablaItems.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"),object:nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Items en carrito"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! CarritoTableViewCell
        
        let item = lista[indexPath.row] as! NSDictionary
 
        let name = item["nombre"] as! String
        let precio = item["precio"] as! Int
        let imageName = item["imagen"] as! String

        
        cell.name.text = name
        cell.price.text = String(precio)
        if let itemImage = imageCache[imageName] {
            cell.itemImage.image = itemImage
        } else {
            let bm = BackendManager()
            bm.downloadImages(imageName, imageR: { (image) in
                DispatchQueue.main.async{
                    cell.itemImage.image = image
                    self.tablaItems.reloadData()
                }
            })
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        print(indexPath)
        selectedIndexPath = indexPath
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let item = lista[selectedIndexPath.row] as! NSDictionary
        let id = item["id"] as! Int
        let nombre = item["nombre"] as! String
        let descripcion = item["descripcion"] as! String
        let detalle = item["detalle"] as! String
        let precio = item["precio"] as! Int
        let imagen = item["imagen"] as! String


        let destination = segue.destination as! DetailedViewController
        
        destination.id = id
        destination.name = nombre
        destination.precio = precio
        destination.detalle = detalle
        destination.descripcion = descripcion
        destination.imagen = imagen
        
    }

}
