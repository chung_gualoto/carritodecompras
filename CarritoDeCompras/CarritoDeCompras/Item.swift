//
//  Item.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 15/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import Foundation
import ObjectMapper

class Item:NSObject, Mappable {
    
    var id: Int?
    var nombre: String?
    var descripcion: String?
    var detalle: String?
    var imagen: String?
    var precio: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nombre <- map["name"]
        descripcion <- map["plaintext"]
        detalle <- map["sanitizedDescription"]
        imagen <- map["image.full"]
        precio <- map["gold.total"]
        
    }
}
