//
//  DetailedViewController.swift
//  CarritoDeCompras
//
//  Created by Patricio Chavez on 6/14/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var id: Int = 0
    var name: String = ""
    var precio: Int = 0
    var descripcion: String = ""
    var detalle: String = ""
    var imagen: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
        priceLabel.text = "Precio: \(String(precio)) de oro"
        descriptionLabel.text = descripcion
        detailLabel.text = detalle
        let bm = BackendManager()
        if let itemImage = imageCache[imagen] {
            self.imageView.image = itemImage
        } else {
            bm.downloadImages(imagen, imageR: { (image) in
                DispatchQueue.main.async{
                    self.imageView.image = image
                }
            })
        }
    }
    
    @IBAction func deleteItem(_ sender: Any) {
        let bm = BackendManager()
        print(id)
        bm.delete(id)
        bm.listarItems()
        (sender as! UIButton).isHidden = true
    }
    
    @IBAction func saveItem(_ sender: Any) {
        let bm = BackendManager()
        print("guardandoitem")
        bm.guardarItem(id: id,nombre: name, descripcion: descripcion, detalle: detalle, imagen: imagen, precio: precio)
        bm.obtenerItems()
        (sender as! UIButton).isHidden = true
    }

}
