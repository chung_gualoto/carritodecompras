//
//  ListaProductosViewController.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 8/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import UIKit

class ListaProductosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating {

    var lista:NSArray = []
    var selectedIndexPath:IndexPath = []
    
    @IBOutlet weak var tablaItems: UITableView!
    
    var listaFiltrada:NSArray = []
    var isSearchActive = false
    
    var resultSearchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confSearchbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizarLista"), object: nil)
        
        let bm = BackendManager()
        bm.obtenerItems()
        
    }
    
    func confSearchbar() {
        resultSearchController = UISearchController(searchResultsController: nil)
        resultSearchController.searchResultsUpdater = self
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.searchBarStyle = .prominent
        resultSearchController.searchBar.sizeToFit()
        tablaItems.tableHeaderView = resultSearchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text != "" {
            isSearchActive = true
            var searchText = searchController.searchBar.text?.lowercased()
            let resultPredicate = NSPredicate(format: "nombre contains[cd] %@", searchText!)
            self.listaFiltrada = self.lista.filtered(using: resultPredicate) as NSArray
        } else {
            isSearchActive = false
        }
        tablaItems.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        tablaItems.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
    }
    
    func actualizarInformacion(_ notification: Notification){
        print("actualizando")
        lista=notification.userInfo?["lista"] as! NSArray
        print(lista.count)
        self.tablaItems.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizarLista"),object:nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive {
            return listaFiltrada.count
        } else {
            return lista.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! CarritoTableViewCell
        
        var item: Item
        
        if isSearchActive {
            item = listaFiltrada[indexPath.row] as! Item
        } else {
            item = lista[indexPath.row] as! Item
        }
        
        let name = item.nombre ?? "???"
        let precio = item.precio! as Int
        let imageName = item.imagen ?? ""
        
        
        cell.name.text = name
        cell.price.text = String(precio)
        
        if let itemImage = imageCache[imageName] {
            cell.itemImage.image = itemImage
        } else {
            let bm = BackendManager()
            bm.downloadImages(imageName, imageR: { (image) in
                DispatchQueue.main.async{
                    cell.itemImage.image = image
                    self.tablaItems.reloadData()
                }
            })
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        print(indexPath)
        selectedIndexPath = indexPath
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let item = lista[selectedIndexPath.row] as! Item
        
        let id = item.id! as Int
        let nombre = item.nombre! as String
        let precio = item.precio! as Int
        let imagen = item.imagen! as String
        let descripcion = item.descripcion ?? " "
        let detalle = item.detalle ?? " "
        
        let destination = segue.destination as! DetailedViewController
        
        destination.id = id
        destination.name = nombre
        destination.precio = precio
        destination.detalle = detalle
        destination.descripcion = descripcion
        destination.imagen = imagen
        
    }

}
