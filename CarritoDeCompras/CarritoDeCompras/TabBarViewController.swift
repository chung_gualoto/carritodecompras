//
//  TabBarViewController.swift
//  CarritoDeCompras
//
//  Created by Edison Chung on 8/6/17.
//  Copyright © 2017 Edison Chung. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let items = tabBar.items
        
        items?[0].title = "Lista de productos"
        items?[0].image = #imageLiteral(resourceName: "icons8-Lista con viñetas-100")
        
        items?[1].title = "Carrito de compras"
        items?[1].image = #imageLiteral(resourceName: "icons8-Carrito de compras-100")
        
    }

}
